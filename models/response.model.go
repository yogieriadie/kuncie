package models

// "time"

// ResponsePagination .. pagination
type ResponsePagination struct {
	TotalRow int         `json:"totalRow"`
	Page     int         `json:"page"`
	Count    int         `json:"count"`
	Contents interface{} `json:"contents"`
	Error    string      `json:"error"`
}

// ContentResponse ...
type ContentResponse struct {
	ErrCode  string      `json:"errCode"`
	ErrDesc  string      `json:"errDesc"`
	Contents interface{} `json:"contents"`
}
