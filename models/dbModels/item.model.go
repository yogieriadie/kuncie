package dbmodels

// Item ...
type Item struct {
	SKU          string  `json:"sku" gorm:"column:sku"`
	Name         string  `json:"name" gorm:"column:name"`
	Price        float64 `json:"price" gorm:"column:price"`
	InventoryQty int     `json:"inventoryQty" gorm:"column:Inventory_qty"`
}

// TableName ...
func (t *Item) TableName() string {
	return "public.m_item"
}
