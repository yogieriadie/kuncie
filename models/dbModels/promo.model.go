package dbmodels

// Promo ...
type Promo struct {
	ID       int64  `json:"id" gorm:"column:id"`
	SKU      string `json:"sku" gorm:"column:sku"`
	Qty      int    `json:"qty" gorm:"column:qty"`
	Operand  string `json:"operand" gorm:"column:operand"`
	Ammount  int    `json:"ammount" gorm:"column:ammount"`
	SKUPromo string `json:"skuPromo" gorm:"column:sku_promo"`
	QtyPromo int    `json:"qtyPromo" gorm:"column:qty_promo"`
}

// TableName ...
func (t *Promo) TableName() string {
	return "public.m_promo"
}
