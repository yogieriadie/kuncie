package constants

// ERR code Global
const (
	ERR_CODE_00     = "00"
	ERR_CODE_00_MSG = "SUCCESS.."

	ERR_CODE_03     = "03"
	ERR_CODE_03_MSG = "Error, unmarshall body Request"
)

// ERR code Global
const (
	ERR_CODE_30     = "30"
	ERR_CODE_30_MSG = "Failed save data to DB"
)

const (
	ERR_CODE_50     = "50"
	ERR_CODE_50_MSG = "Invalid username / password"

	ERR_CODE_51     = "51"
	ERR_CODE_51_MSG = "Error connection to database"

	ERR_CODE_53     = "53"
	ERR_CODE_53_MSG = "Failed generate token !"

	ERR_CODE_54     = "54"
	ERR_CODE_54_MSG = "Invalid Authorization !"

	ERR_CODE_55     = "55"
	ERR_CODE_55_MSG = "Token expired !"

	ERR_CODE_61     = "61"
	ERR_CODE_61_MSG = "User not found !"

	ERR_CODE_62     = "62"
	ERR_CODE_62_MSG = "Password not match !"

	ERR_CODE_63     = "63"
	ERR_CODE_63_MSG = "Failed Update password !"
)

const (
	ERR_CODE_40     = "40"
	ERR_CODE_40_MSG = "Data not found"
)
