package main

import (
	_ "github.com/jinzhu/gorm/dialects/postgres"

	_ "kuncie/database"
	routers "kuncie/routers"

	"github.com/astaxie/beego"
	"github.com/gin-gonic/gin"
)

// @securityDefinitions.apikey ApiKeyAuth
// @in header
// @name Authorization
func main() {
	runMode := beego.AppConfig.DefaultString("gin.mode", "debug")
	serverPort := beego.AppConfig.DefaultString("httpport", "8080")

	gin.SetMode(runMode)
	routersInit := routers.InitRouter()
	routersInit.Run(":" + serverPort)

}
