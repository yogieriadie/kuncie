package controllers

import (
	"encoding/json"
	"io/ioutil"
	"kuncie/constants"
	"kuncie/models"
	"kuncie/models/dto"
	"kuncie/services"
	"net/http"

	"github.com/gin-gonic/gin"
	"github.com/jinzhu/gorm"
)

// CheckoutController ...
type CheckoutController struct {
	DB *gorm.DB
}

// CheckoutService ...
var CheckoutService = new(services.CheckoutService)

func (h *CheckoutController) GetTotal(c *gin.Context) {
	res := models.ContentResponse{}
	req := dto.CheckoutParam{}

	body := c.Request.Body
	dataBodyReq, _ := ioutil.ReadAll(body)

	if err := json.Unmarshal(dataBodyReq, &req); err != nil {
		res.ErrCode = constants.ERR_CODE_03
		res.ErrDesc = constants.ERR_CODE_03_MSG
		c.JSON(http.StatusBadRequest, res)
		c.Abort()
	}

	total, err := CheckoutService.Checkout(req)
	if err != nil {
		res.ErrCode = constants.ERR_CODE_03
		res.ErrDesc = constants.ERR_CODE_03_MSG
		c.JSON(http.StatusBadRequest, res)
		c.Abort()
	}

	res.Contents = total
	res.ErrCode = constants.ERR_CODE_00
	res.ErrDesc = constants.ERR_CODE_00_MSG

	c.JSON(http.StatusOK, res)
	c.Abort()
}
