package services

import (
	"kuncie/database"
	// dbmodels "kuncie/models/dbModels"
)

// ItemService ...
type CheckoutService struct {
}

// GetBusinessUnitPaging ...
func (h CheckoutService) Checkout(param []string) (float32, error) {
	var total float32
	for _, sku := range param {
		item, err := database.GetPricebySKU(sku)
		if err != nil {
			return total, err
		}
		total += item

	}

	for _, sku := range param {
		promos, err := database.GetPromobySKU(sku)
		if err != nil {
			return total, err
		}
		if len(promos) > 0 {
			for _, promo := range promos {
				if (promo.Ammount != 0) && (GetTotalSKUItem(param, sku) >= promo.Qty) {
					disc := float32(promo.Ammount) / 100
					total = total - (total * disc)
					return total, err
				} else {
					if (GetTotalSKUItem(param, sku) >= promo.Qty) && (promo.SKUPromo != "") && (GetTotalSKUItem(param, promo.SKUPromo) > 0) {
						priceDeduct, err := database.GetPricebySKU(promo.SKUPromo)
						if err != nil {
							return total, err
						}
						disc := priceDeduct * float32(promo.QtyPromo)
						total = total - disc
						return total, err
					}
				}
			}
		}
	}

	return total, nil
}

func GetTotalSKUItem(skus []string, sku string) int {
	total := 0
	for _, n := range skus {
		if sku == n {
			total += 1
		}
	}
	return total
}
