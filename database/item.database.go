package database

import (
	dbmodels "kuncie/models/dbModels"
)

func GetPricebySKU(sku string) (float32, error) {
	item := dbmodels.Item{}
	db := GetDbCon()
	db.Debug().LogMode(true)
	err := db.Model(&dbmodels.Item{}).Where("SKU=?", &sku).First(&item).Error
	return float32(item.Price), err
}
