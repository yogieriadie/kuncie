package database

import (
	dbmodels "kuncie/models/dbModels"
)

func GetPromobySKU(sku string) ([]dbmodels.Promo, error) {
	promo := []dbmodels.Promo{}
	db := GetDbCon()
	db.Debug().LogMode(true)
	err := db.Model(&dbmodels.Promo{}).Where("SKU=?", &sku).First(&promo).Error

	return promo, err
}
