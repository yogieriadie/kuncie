CREATE TABLE public.m_item (
	sku varchar NOT NULL,
	"name" varchar NULL,
	price float8 NULL,
	inventory_qty int4 NULL,
	CONSTRAINT m_item_pk PRIMARY KEY (sku)
);

INSERT INTO public.m_item (sku,"name",price,inventory_qty) VALUES
	 ('120P90','Google Home',49.99,10),
	 ('43N23P','MacBook Pro',5399.99,5),
	 ('A304SD','Alexa Speaker',109.5,10),
	 ('234234','Raspberry Pi B',30.0,2);

CREATE TABLE public.m_promo (
	id bigserial NOT NULL,
	sku varchar NULL,
	qty int4 NULL,
	operand varchar NULL,
	ammount int4 NULL,
	sku_promo varchar NULL,
	qty_promo varchar NULL,
	CONSTRAINT m_promo_pk PRIMARY KEY (id)
);

INSERT INTO public.m_promo (sku,qty,operand,ammount,sku_promo,qty_promo) VALUES
	 ('43N23P',1,'equal',NULL,'234234','1'),
	 ('120P90',3,'equal',NULL,'120P90','1'),
	 ('A304SD',3,'more',10,NULL,NULL);
